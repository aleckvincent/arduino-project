const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// body parsing
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// routing
require('./app/routes/routing.js')(app);

// server
const port = process.env.PORT || '3000';
app.listen(port, () => {
  console.log('Server listening on port ' + port);
});

